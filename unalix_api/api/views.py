from django.views.decorators.http import require_http_methods
from django.http.response import (
	JsonResponse,
	HttpResponseBadRequest,
	HttpResponsePermanentRedirect
)

from unalix import clear_url
from httpx._exceptions import (
	ConnectError,
	HTTPStatusError
)

@require_http_methods(["GET"])
def parse_links(request):
	
	# If the request has a parameter that we don't expect to receive, we return the
	# 400 (Bad Request) status code to it.
	for param in request.GET.keys():
		if param not in ['url', 'auto_redirect']:
			return HttpResponseBadRequest()
	
	# Get value of the "auto_redirect" parameter
	auto_redirect = request.GET.get('auto_redirect', False)
	
	# Get value of the "url" parameter
	url = request.GET.get('url', None)
	
	if len(str(url)) == 0 or url == None:
		return HttpResponseBadRequest()
	
	try:
		# Make a GET request to the url and follow 301/302 redirects (if any)
		url = clear_url(url)
	except ( ConnectError, HTTPStatusError ) as e:
		url = str(e.request.url)
	
	if auto_redirect == 'true':
		return HttpResponsePermanentRedirect(url)
	else:
		return JsonResponse(dict(url=url), json_dumps_params=dict(indent=4))
	