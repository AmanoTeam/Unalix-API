from django.utils.deprecation import MiddlewareMixin
from django.http import HttpResponsePermanentRedirect

class Middleware(MiddlewareMixin):

	def process_request(self, request):
		self.headers = [
			("Cache-Control", "no-cache, no-store, must-revalidate"),
			("Content-Security-Policy", "default-src 'none'"),
			("Referrer-Policy", "no-referrer"),
			("X-Content-Type-Options", "nosniff"),
			("X-Frame-Options", "DENY"),
			("X-XSS-Protection", "1; mode=block"),
		]
		
		if ( 'Upgrade-Insecure-Requests' in request.headers.keys() and not request.is_secure() ):
			self.headers += [('Vary', 'Upgrade-Insecure-Requests')]
			return HttpResponsePermanentRedirect(
				"https://{}{}".format(request.get_host(), request.get_full_path())
			)
		
		if request.is_secure():
			self.headers += [("Strict-Transport-Security", "max-age=31536000; includeSubDomains; preload")]
		
		print(self.headers)

	def process_response(self, request, response):
		
		self.headers += [('Content-Length', len(response.content))]
		
		for key, value in self.headers:
			response.setdefault(key, value)
		
		return response
